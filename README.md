# Établi Silicon Academy

- [Établis disponibles](#établis-disponibles)
- [Téléchargement](#téléchargement)
- [Déploiement](#déploiement)
- [Connexion à la machine](#connexion-à-la-machine)
- [Volume partagé](#volume-partagé)

### Établis disponibles

- Établi SPICE
- Établi RISC-V
- Établi OpenROAD


### Téléchargement

Récupération du repo, et de ses sous-modules :
```bash
git clone --recurse-submodules "git@gitlab.com:le.concepteur/etabli_silicon_academy.git" mon_etabli
cd mon_etabli/
```

Si par mégarde vous n'avez pas `git clone` avec l'option `--recurse-submodules`, vous pouvez :
```bash
git submodule init
git submodule update
```

À ce stade, vous devriez avoir ces dossiers existants et non-vides, contenant les sources des modules :
- `./containers/etabli_spice/ngspice-latest/`
- `./containers/etabli_openroad/OpenROAD/`
- `./containers/etabli_openroad/OpenROAD/third-party/abc/`
- `./containers/etabli_openroad/OpenROAD/src/sta/`


### Déploiement

Vous pouvez déployer un établi spécifique (en précisant son nom):

```bash
docker-compose build etabli-spice
docker-compose up -d etabli-spice
```

### Connexion à la machine

On lance un interpréteur de commande sur la machine (ici bash) :

```bash
docker exec -it etabli-spice bash
```

### Volume partagé

- Le dossier `hdd` (hôte) est monté en volume sur le container dans le répertoire /HDD
